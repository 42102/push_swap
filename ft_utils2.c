/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/01 17:45:31 by anramire          #+#    #+#             */
/*   Updated: 2022/06/02 01:16:00 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	update_str(char *str, char *buffer, int *i, int *flag_num)
{	
	buffer[(*i)++] = *str;
	(*flag_num) = 1;
}

void	get_stack(char **argv, int argc, int **stack_a, int *len)
{
	if (argc == 2)
	{
		*stack_a = get_stack_from_str(argv[1], len);
	}
	else if (argc > 2)
	{
		*stack_a = get_stack_from_multiple_str(argv, argc, len);
	}
	else
	{
		ft_putstr_fd("Error\n", 1);
	}
}

void	is_sorted_checker(int *stack, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		if (i != stack[i])
			break ;
		i++;
	}
	if (i == len)
	{
		ft_putstr_fd("OK\n", 1);
	}
	else
	{
		ft_putstr_fd("KO\n", 1);
	}
	exit(-1);
}

int	ft_strcmp(const char *s1, const char *s2)
{
	size_t				i;	
	const unsigned char	*p1 = (const unsigned char *)s1;
	const unsigned char	*p2 = (const unsigned char *)s2;

	i = 0;
	while ((p1[i] != '\0') && (p2[i] != '\0'))
	{
		if (p1[i] != p2[i])
			return (p1[i] - p2[i]);
		i++;
	}
	if ((p1[i] == '\0') || (p2[i] == '\0'))
		return (p1[i] - p2[i]);
	return (0);
}

void	do_b_commands(int *stack_b, int *len_b, char *cmd)
{
	if (ft_strcmp(cmd, "rrb\n") == 0)
		reverse_rotate_modified(stack_b, *len_b);
	if (ft_strcmp(cmd, "sb\n") == 0)
		swap_modified(stack_b, *len_b);
	if (ft_strcmp(cmd, "rb\n") == 0)
		rotate_modified(stack_b, *len_b);
}
