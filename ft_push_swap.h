/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_swap.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/24 23:51:11 by anramire          #+#    #+#             */
/*   Updated: 2022/06/02 01:14:30 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PUSH_SWAP_H
# define FT_PUSH_SWAP_H
# include "libft/libft.h"

int		check_str(char *str);
int		*get_stack_from_str(char *str, int *len);
long	m_atoi(const char *nptr);
int		*get_stack_from_multiple_str(char **argv, int num_args, int *len);
void	get_stack(char **argv, int argc, int **stack_a, int *len);
//Movements
void	swap(int *stack, int len, int id_stack);//sa and sb
void	swapss(int *stack_a, int *stack_b, int len_a, int len_b);//ss
void	push_a(int *stack_a, int *stack_b, int *len_a, int *len_b);//pa
void	push_b(int *stack_a, int *stack_b, int *len_a, int *len_b);//pb
void	rotate(int *stack, int len, int id_stack);//ra and rb
void	rotate_both(int *stack_a, int *stack_b, int len_a, int len_b);//rr
void	reverse_rotate(int *stack, int len, int id_stack);//rra and rrb
void	reverse_rotate_both(int *stack_a, int *stack_b, int len_a, int len_b);

//Movements with no output
void	swap_modified(int *stack, int len);
void	swapss_modified(int *stack_a, int *stack_b, int len_a, int len_b);
void	push_a_modified(int *stack_a, int *stack_b, int *len_a, int *len_b);
void	push_b_modified(int *stack_a, int *stack_b, int *len_a, int *len_b);
void	rotate_modified(int *stack, int len);
void	rotate_both_mod(int *stack_a, int *stack_b, int len_a, int len_b);
void	reverse_rotate_modified(int *stack, int len);
void	reverse_rotate_both_m(int *stack_a, int *stack_b, int len_a, int len_b);
//Algorithms
void	sort_3_numbers(int *stack_a, int len_a);
void	sort(int *stack_a, int *stack_b, int *len_a, int *len_b);
void	case_4_numbers(int *stack_a, int *stack_b, int *len_a, int *len_b);
void	sort_lot_of_numbers(int *stack_a, int *stack_b, int *len_a, int *len_b);

//utils
int		*initialize_stack(int len);
void	check_stack(int *stack, int len);
void	converse_number(int *stack, int len);
int		get_min(int *stack, int len);
void	is_sorted(int *stack, int len);
void	update_str(char *str, char *buffer, int *i, int *flag_num);
void	check_stack_a_5_numbers(int *stack_a, int *len_a, int max);
void	is_sorted_checker(int *stack, int len);
int		ft_strcmp(const char *s1, const char *s2);
void	do_b_commands(int *stack_b, int *len_b, char *cmd);

//Aux to delete when it´s finnished
void	show_stack(int *stack, int len, int id_stack);

#endif
