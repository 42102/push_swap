/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_movements2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/30 23:23:40 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 18:10:27 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	rotate_both(int *stack_a, int *stack_b, int len_a, int len_b)
{
	int	aux;
	int	i;

	ft_putstr_fd("rr\n", 1);
	i = 0;
	aux = stack_a[0];
	while (i < (len_a - 1))
	{
		stack_a[i] = stack_a[i + 1];
		i++;
	}
	stack_a[len_a - 1] = aux;
	i = 0;
	aux = stack_b[0];
	while (i < (len_b - 1))
	{
		stack_b[i] = stack_b[i + 1];
		i++;
	}
	stack_b[len_b - 1] = aux;
}

void	reverse_rotate(int *stack, int len, int id_stack)
{
	int	aux;
	int	i;

	if (id_stack == 1)
		ft_putstr_fd("rra\n", 1);
	if (id_stack == 0)
		ft_putstr_fd("rrb\n", 1);
	i = len - 1;
	aux = stack[i];
	while (i > 0)
	{
		stack[i] = stack[i - 1];
		i--;
	}
	stack[0] = aux;
}

void	reverse_rotate_both(int *stack_a, int *stack_b, int len_a, int len_b)
{
	int	aux;
	int	i;

	ft_putstr_fd("rrr\n", 1);
	i = len_a - 1;
	aux = stack_a[i];
	while (i > 0)
	{
		stack_a[i] = stack_a[i - 1];
		i--;
	}
	stack_a[0] = aux;
	i = len_b - 1;
	aux = stack_b[i];
	while (i > 0)
	{
		stack_b[i] = stack_b[i - 1];
		i--;
	}
	stack_b[0] = aux;
}
