#---------------------------- Makefile ---------------------------

SRC = push_swap.c parser.c ft_str_to_number.c modified_atoi.c aux.c ft_utils.c\
	  ft_movements1.c ft_movements2.c algorithms1.c algorithms1_utils.c\
		algorithms2.c ft_utils2.c ft_movements1_modified.c\
		ft_movements2_modified.c
SRC_BONUS = checker.c parser.c ft_str_to_number.c modified_atoi.c aux.c ft_utils.c\
	  ft_movements1.c ft_movements2.c algorithms1.c algorithms1_utils.c\
		algorithms2.c ft_utils2.c ft_movements2_modified.c\
		ft_movements1_modified.c
 
OBJS = $(SRC:.c=.o)
OBJS_BONUS = $(SRC_BONUS:.c=.o)
NAME = push_swap
NAME_BONUS = checker
CC = gcc
CFLAGS = -Wall -Werror -Wextra

%.o: %.c
	$(CC) $(CFLAGS) -g -c $^

all: $(NAME)

re: fclean
	$(MAKE)

bonus: $(NAME) $(OBJS_BONUS)
		$(CC) $(CFLAGS) -Llibft -lft -Lft_get_next_line -lnext_line -g -o $(NAME_BONUS) $(OBJS_BONUS)

$(NAME): libft.a libnext_line.a $(OBJS)
	$(CC) $(CFLAGS) -Llibft -lft -g -o $(NAME) $(OBJS)

libft.a:
	make -C libft

libnext_line.a:
	make -C ft_get_next_line
clean:
	rm -rf *.o
	make clean -C ./libft
	make clean -C ./ft_get_next_line

fclean: clean
	rm -rf $(NAME) $(NAME_BONUS)
	make fclean -C ./libft
	make fclean -C ./ft_get_next_line


.PHONY: clean fclean re all bonus
