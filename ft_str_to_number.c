/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_to_number.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/26 15:23:37 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 17:49:13 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//#include <stdio.h>
#include "ft_push_swap.h"

static int	*initialize_variables(char *str, int *flg_num, int *i, int *len);
static void	check_last_element(int *stack, int i);

static void	check_modified_atoi(char *buffer, int **stack, int i, int *len)
{
	buffer[i] = '\0';
	if (m_atoi(buffer) > 2147483647 || (m_atoi(buffer) < -2147483648))
	{
		free(*stack);
		ft_putstr_fd("Error\n", 2);
		exit(-1);
	}
	(*stack)[(*len)++] = m_atoi(buffer);
}

int	*get_stack_from_str(char *str, int *len)
{
	int		*stack;
	char	buffer[50];
	int		i;
	int		flag_num;

	stack = initialize_variables(str, &flag_num, &i, len);
	while (*str != '\0')
	{
		if (((*str >= '0') && (*str <= '9')) || (*str == '-'))
			update_str(str, buffer, &i, &flag_num);
		else
		{
			if (flag_num == 1)
				check_modified_atoi(buffer, &stack, i, len);
			i = 0;
			flag_num = 0;
		}
		str++;
	}	
	check_last_element(stack, i);
	if (flag_num == 1 && (*str == '\0'))
		check_modified_atoi(buffer, &stack, i, len);
	return (stack);
}

static int	*initialize_variables(char *str, int *flg_num, int *i, int *len)
{
	int	numbers;

	*flg_num = 0;
	*i = 0;
	*len = 0;
	numbers = check_str(str);
	if (numbers <= -1)
	{
		ft_putstr_fd("Error\n", 2);
		exit(-1);
	}
	return (initialize_stack(numbers));
}

int	*get_stack_from_multiple_str(char **argv, int num_args, int *len)
{
	int		i;
	char	*str_aux;
	char	*str;

	str = (char *)malloc(sizeof(char));
	if (str == NULL)
		return (NULL);
	i = 1;
	while (i < num_args)
	{	
		str_aux = str;
		str = ft_strjoin(str, argv[i]);
		free(str_aux);
		str_aux = str;
		str = ft_strjoin(str, " ");
		free(str_aux);
		i++;
	}
	return (get_stack_from_str(str, len));
}

static void	check_last_element(int *stack, int i)
{
	if (i > 11)
	{
		ft_putstr_fd("Error\n", 2);
		free(stack);
		exit(-1);
	}
}
