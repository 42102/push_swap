/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithms2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/31 23:36:06 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 18:59:48 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"
#include <stdio.h>

int			get_exp(int len);
static void	init_var(int *len_t, int *len_a, int *iterations, int *i);

void	sort_lot_of_numbers(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	int	iterations;
	int	i;
	int	j;
	int	len_t;

	init_var(&len_t, len_a, &iterations, &i);
	while (i < iterations)
	{
		j = 0;
		while (j < (len_t))
		{
			if (((stack_a[0] >> i) & 1) == 0)
			{
				push_b(stack_a, stack_b, len_a, len_b);
			}
			else
			{
				rotate(stack_a, *len_a, 1);
			}
			j++;
		}
		while (*len_b > 0)
			push_a(stack_a, stack_b, len_a, len_b);
		i++;
	}
}

int	get_exp(int len)
{
	int	i;
	int	number;

	i = 1;
	number = 2;
	while (number < len)
	{
		number = number * 2;
		i++;
	}
	return (i);
}

static void	init_var(int *len_t, int *len_a, int *iterations, int *i)
{
	(*len_t) = (*len_a);
	(*iterations) = get_exp(*len_a);
	(*i) = 0;
}
