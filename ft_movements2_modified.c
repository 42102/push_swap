/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_movements2_modified.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/30 23:23:40 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 22:45:37 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	rotate_both_mod(int *stack_a, int *stack_b, int len_a, int len_b)
{
	int	aux;
	int	i;

	i = 0;
	aux = stack_a[0];
	while (i < (len_a - 1))
	{
		stack_a[i] = stack_a[i + 1];
		i++;
	}
	stack_a[len_a - 1] = aux;
	i = 0;
	aux = stack_b[0];
	while (i < (len_b - 1))
	{
		stack_b[i] = stack_b[i + 1];
		i++;
	}
	stack_b[len_b - 1] = aux;
}

void	reverse_rotate_modified(int *stack, int len)
{
	int	aux;
	int	i;

	i = len - 1;
	aux = stack[i];
	while (i > 0)
	{
		stack[i] = stack[i - 1];
		i--;
	}
	stack[0] = aux;
}

void	reverse_rotate_both_m(int *stack_a, int *stack_b, int len_a, int len_b)
{
	int	aux;
	int	i;

	i = len_a - 1;
	aux = stack_a[i];
	while (i > 0)
	{
		stack_a[i] = stack_a[i - 1];
		i--;
	}
	stack_a[0] = aux;
	i = len_b - 1;
	aux = stack_b[i];
	while (i > 0)
	{
		stack_b[i] = stack_b[i - 1];
		i--;
	}
	stack_b[0] = aux;
}
