/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithms1_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/31 22:53:57 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 18:34:13 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	case_4_numbers(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	int	min;

	min = get_min(stack_a, *len_a);
	if (min == 0)
		push_b(stack_a, stack_b, len_a, len_b);
	if (min == 1)
	{
		swap(stack_a, *len_a, 1);
		push_b(stack_a, stack_b, len_a, len_b);
	}
	if (min == 3)
	{
		reverse_rotate(stack_a, *len_a, 1);
		push_b(stack_a, stack_b, len_a, len_b);
	}
	if (min == 2)
	{
		reverse_rotate(stack_a, *len_a, 1);
		reverse_rotate(stack_a, *len_a, 1);
		push_b(stack_a, stack_b, len_a, len_b);
	}
	sort_3_numbers(stack_a, *len_a);
	push_a(stack_a, stack_b, len_a, len_b);
}

void	check_stack_a_5_numbers(int *stack_a, int *len_a, int max)
{
	if (max > 0)
	{
		if (max == 1)
			swap(stack_a, *len_a, 1);
		if (max == 2)
		{
			rotate(stack_a, *len_a, 1);
			rotate(stack_a, *len_a, 1);
		}
		if (max == 3)
		{
			reverse_rotate(stack_a, *len_a, 1);
			reverse_rotate(stack_a, *len_a, 1);
		}
		if (max == 4)
			reverse_rotate(stack_a, *len_a, 1);
	}
}
