/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_push_swap.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/24 23:54:47 by anramire          #+#    #+#             */
/*   Updated: 2022/05/26 15:20:58 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

static int	check_conditions(char c, int *space, int *min, int *flg_num);

int	check_str(char *str)
{
	int	aux;
	int	space;
	int	number;
	int	minus;
	int	flag_number;

	flag_number = 0;
	minus = 0;
	number = 0;
	space = 1;
	while (*str != '\0')
	{
		aux = check_conditions(*str, &space, &minus, &flag_number);
		if (aux == -1)
			return (aux);
		number += aux;
		str++;
	}
	return (number);
}

static int	check_conditions(char c, int *space, int *min, int *flg_num)
{
	if ((c != ' ') && !((c >= '0' && (c <= '9')) || (c == '-')))
		return (-1);
	if (c == ' ')
	{
		(*space) = 1;
		if ((*flg_num) == 1)
			(*flg_num) = 0;
	}
	if ((*min) == 1 && !(c >= '0' && (c <= '9')))
		return (-1);
	if (c == '-')
	{
		if ((*flg_num) == 1)
			return (-1);
		(*min) = 1;
	}
	if ((*space) == 1 && (c >= '0' && (c <= '9')))
	{
		(*flg_num) = 1;
		(*space) = 0;
		(*min) = 0;
		return (1);
	}
	return (0);
}
