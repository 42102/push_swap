/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aux.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/30 20:53:32 by anramire          #+#    #+#             */
/*   Updated: 2022/05/31 16:23:01 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"
#include <stdio.h>

void	show_stack(int *stack, int len, int id_stack)
{
	int	i;

	i = 0;
	ft_putstr_fd("Stack ", 1);
	if (id_stack == 1)
		ft_putstr_fd("a: \n", 1);
	if (id_stack == 0)
		ft_putstr_fd("b: \n", 1);
	while (i < len)
	{
		printf("%d\n", stack[i]);
		i++;
	}	
	ft_putstr_fd("------------------------------\n", 1);
}
