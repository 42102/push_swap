/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/09 23:23:49 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 20:22:28 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line_bonus.h"

char	*ft_read(int fd, char *buf);
char	*remove_line(char *str);
char	*get_line(char *buffer);

char	*get_next_line(int fd)
{
	static char	*buffer[256];
	char		*line;

	if (fd < 0 || BUFFER_SIZE < 0 || (read(fd, 0, 0) < 0))
	{
		return (NULL);
	}
	if (!buffer[fd])
	{
	buffer[fd] = (char *)malloc(1 * sizeof(char));
	buffer[fd][0] = '\0';
	}
	buffer[fd] = ft_read(fd, buffer[fd]);
	line = get_line(buffer[fd]);
	buffer[fd] = remove_line(buffer[fd]);
	return (line);
}

char	*ft_read(int fd, char *buf)
{
	char	*aux_buf;
	ssize_t	bytes_read;

	aux_buf = (char *)malloc((BUFFER_SIZE + 1) * sizeof(char));
	if (aux_buf == NULL)
		return (NULL);
	bytes_read = 1;
	while (ft_strchr(buf, '\n') == NULL && (bytes_read != 0))
	{
		bytes_read = read(fd, aux_buf, BUFFER_SIZE);
		if (bytes_read == -1)
		{
			free(aux_buf);
			return (NULL);
		}
		aux_buf[bytes_read] = '\0';
		buf = ft_strjoin1(buf, aux_buf);
	}
	free(aux_buf);
	return (buf);
}

char	*remove_line(char *str)
{
	char	*aux_str;
	int		i;
	int		aux;

	i = 0;
	while (str[i] != '\n' && (str[i]))
		i++;
	if (str[i] == '\0')
	{
		free(str);
		return (NULL);
	}
	aux_str = (char *)malloc((get_length(str) - i + 1) * sizeof(char));
	if (aux_str == NULL)
		return (NULL);
	aux = 0;
	i++;
	while (str[i] != '\0')
	{
		aux_str[aux++] = str[i++];
	}
	aux_str[aux] = '\0';
	free(str);
	return (aux_str);
}

char	*get_line(char *buffer)
{
	int		c;
	int		i;
	char	*aux_buf;

	i = 0;
	if (buffer[i] == '\0')
		return (NULL);
	while (buffer[i] != '\n' && (buffer[i] != '\0'))
		i++;
	aux_buf = (char *)malloc((i + 2) * sizeof(char));
	c = 0;
	while (c <= i)
	{
		aux_buf[c] = buffer[c];
		c++;
	}
	aux_buf[c] = '\0';
	return (aux_buf);
}
