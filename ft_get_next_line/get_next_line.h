/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/09 23:22:30 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 20:57:27 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 50
# endif
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# define TAM_FD 4096

char	*get_next_line(int fd);
void	init_v(size_t *l1, size_t *l2, char *s1, char *s2);
size_t	get_length(const char *str);
char	*ft_strjoin1(char *s1, char const *s2);
char	*ft_strchr1(const char *s, int c);
char	*ft_substr(const char *s, unsigned int start, size_t len);
#endif
