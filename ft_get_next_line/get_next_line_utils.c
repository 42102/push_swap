/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/10 23:50:49 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 20:57:15 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line.h"

char	*ft_strchr1(const char *s, int c)
{
	size_t	len;
	size_t	i;
	char	*aux;

	aux = (char *)s;
	len = get_length(s);
	i = 0;
	while (i <= len)
	{
		if ((*aux) == (char)c)
			return (aux);
		aux++;
		i++;
	}
	return (NULL);
}

char	*ft_strjoin1(char *s1, char const *s2)
{
	char	*dup;
	size_t	len_s1;
	size_t	len_s2;
	size_t	i;

	init_v(&len_s1, &len_s2, (char *)s1, (char *) s2);
	dup = (char *)malloc((len_s1 + len_s2 + 1) * sizeof(char));
	if (dup == NULL)
		return (NULL);
	i = 0;
	while (i < len_s1)
	{
		dup[i] = s1[i];
		i++;
	}
	i = 0;
	while (i < len_s2)
	{
		dup[len_s1 + i] = s2[i];
		i++;
	}
	dup[len_s1 + i] = '\0';
	free(s1);
	return (dup);
}

size_t	get_length(const char *str)
{
	size_t	len;

	len = 0;
	while (str[len] != '\0')
		len++;
	return (len);
}

void	init_v(size_t *l1, size_t *l2, char *s1, char *s2)
{
	*l1 = get_length(s1);
	*l2 = get_length(s2);
}

char	*ft_substr(const char *s, unsigned int start, size_t len)
{
	char	*dup;
	size_t	i;	
	size_t	slen;

	slen = get_length(s);
	if (start > slen)
	{
		dup = malloc(1);
		return (dup);
	}
	if ((slen - start) < len)
		len = slen - start;
	dup = (char *)malloc((len + 1) * sizeof(char));
	if (dup == NULL)
		return (NULL);
	i = 0;
	while (i < len && (s[i] != '\0'))
	{
		dup[i] = s[start + i];
		i++;
	}
	return (dup);
}
