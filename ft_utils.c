/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/30 21:02:57 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 18:13:23 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	organize_array(int *aux_array, int len);

int	*initialize_stack(int len)
{
	int	*stack;

	stack = (int *) malloc(len * sizeof(int));
	if (stack == NULL)
		return (NULL);
	return (stack);
}

void	check_stack(int *stack, int len)
{
	int	i;
	int	j;

	i = 0;
	while (i < len)
	{
		j = i + 1;
		while (j < len)
		{
			if (stack[i] == stack[j])
			{
				ft_putstr_fd("Error\n", 2);
				free(stack);
				exit(-1);
			}
			j++;
		}
		i++;
	}
}

void	converse_number(int *stack, int len)
{
	int	*aux_array;
	int	i;
	int	j;

	i = -1;
	aux_array = initialize_stack(len);
	while (i++ < len)
		aux_array[i] = stack[i];
	organize_array(aux_array, len);
	i = 0;
	while (i < len)
	{
		j = 0;
		while (j < len)
		{
			if (aux_array[j] == stack[i])
			{
				stack[i] = j;
				break ;
			}
			j++;
		}	
		i++;
	}
	free(aux_array);
}

void	organize_array(int *array, int len)
{
	int	i;
	int	j;
	int	aux;

	aux = 0;
	i = 0;
	while (i < len)
	{
		j = 0;
		while (j < len - i - 1)
		{
			if (array[j] > array[j + 1])
			{
				aux = array[j + 1];
				array[j + 1] = array[j];
				array[j] = aux;
			}
			j++;
		}
		i++;
	}
}

void	is_sorted(int *stack, int len)
{
	int	i;

	i = 0;
	while (i < len)
	{
		if (i != stack[i])
			break ;
		i++;
	}
	if (i == len)
	{
		free(stack);
		exit(-1);
	}
}
