/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithms1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/31 15:06:03 by anramire          #+#    #+#             */
/*   Updated: 2022/06/01 18:32:23 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

void	sort(int *stack_a, int *stack_b, int *len_a, int *len_b);
int		get_min(int *stack, int len);
int		get_max(int *stack, int len);

void	sort_3_numbers(int *st, int len_a)
{
	if (len_a == 2)
	{
		if (st[0] > st[1])
			swap(st, len_a, 1);
	}	
	if (len_a == 3)
	{
		if ((st[0] > st[1]) && (st[0] < st[2]) && (st[1] < st[2]))
			swap(st, len_a, 1);
		if (st[0] > st[1] && (st[1] > st[2]))
		{
			swap(st, len_a, 1);
			reverse_rotate(st, len_a, 1);
		}
		if ((st[0] > st[1]) && (st[0] > st[2]) && (st[1] < st[2]))
			rotate(st, len_a, 1);
		if ((st[0] < st[1]) && (st[0] < st[2]) && (st[1] > st[2]))
		{
			swap(st, len_a, 1);
			rotate(st, len_a, 1);
		}
		if ((st[0] < st[1]) && (st[0] > st[2]) && (st[1] > st[2]))
			reverse_rotate(st, len_a, 1);
	}
}

void	sort_5_numbers(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	int	max;

	if ((*len_a) == 4)
	{
		case_4_numbers(stack_a, stack_b, len_a, len_b);
		return ;
	}
	max = get_max(stack_a, *len_a);
	check_stack_a_5_numbers(stack_a, len_a, max);
	push_b(stack_a, stack_b, len_a, len_b);
	case_4_numbers(stack_a, stack_b, len_a, len_b);
	push_a(stack_a, stack_b, len_a, len_b);
	rotate(stack_a, *len_a, 1);
}

void	sort(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	is_sorted(stack_a, *len_a);
	if ((*len_a) <= 3)
		sort_3_numbers(stack_a, *len_a);
	if (((*len_a) > 3) && ((*len_a) <= 5))
		sort_5_numbers(stack_a, stack_b, len_a, len_b);
	if ((*len_a) > 5)
		sort_lot_of_numbers(stack_a, stack_b, len_a, len_b);
}

int	get_min(int *stack, int len)
{
	int	min;
	int	i;

	i = 1;
	min = 0;
	while (i < len)
	{
		if (stack[i] < stack[min])
			min = i;
		i++;
	}
	return (min);
}

int	get_max(int *stack, int len)
{
	int	max;
	int	i;

	i = 1;
	max = 0;
	while (i < len)
	{
		if (stack[i] > stack[max])
			max = i;
		i++;
	}
	return (max);
}
