/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/30 20:39:44 by anramire          #+#    #+#             */
/*   Updated: 2022/06/02 19:39:01 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft/libft.h"
#include "ft_push_swap.h"

int	main(int argc, char *argv[])
{	
	int	len_a;
	int	len_b;
	int	*stack_a;
	int	*stack_b;

	if (argc > 1)
	{
		get_stack(argv, argc, &stack_a, &len_a);
		check_stack(stack_a, len_a);
		stack_b = initialize_stack(len_a);
		len_b = 0;
		converse_number(stack_a, len_a);
		sort(stack_a, stack_b, &len_a, &len_b);
		free(stack_a);
		free(stack_b);
	}
	return (0);
}
