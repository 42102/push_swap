/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_movements1_modified.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/30 20:50:35 by anramire          #+#    #+#             */
/*   Updated: 2022/06/02 00:50:36 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"

//id_stack == 1 ==> stack_a
//id_stack == 0 ==> stack_b
void	swap_modified(int *stack, int len)
{
	int	aux;

	if (len > 1)
	{
		aux = stack[0];
		stack[0] = stack[1];
		stack[1] = aux;
	}
}

void	swapss_modified(int *stack_a, int *stack_b, int len_a, int len_b)
{
	int	aux;

	if (len_a > 1)
	{
		aux = stack_a[0];
		stack_a[0] = stack_a[1];
		stack_a[1] = aux;
	}
	if (len_b > 1)
	{
		aux = stack_b[0];
		stack_b[0] = stack_b[1];
		stack_b[1] = aux;
	}	
}

void	push_a_modified(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	int	i;

	if ((*len_b) > 0)
	{
		i = (*len_a);
		while (i > 0)
		{
			stack_a[i] = stack_a[i - 1];
			i--;
		}
		stack_a[(0)] = stack_b[(0)];
		i = 0;
		while (i < (*len_b))
		{
			stack_b[i] = stack_b[i + 1];
			i++;
		}
		(*len_a)++;
		(*len_b)--;
	}
}

void	push_b_modified(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	int	i;

	if ((*len_a) > 0)
	{
		i = (*len_b);
		while (i > 0)
		{
			stack_b[i] = stack_b[i - 1];
			i--;
		}
		stack_b[(0)] = stack_a[(0)];
		i = 0;
		while (i < (*len_a))
		{
			stack_a[i] = stack_a[i + 1];
			i++;
		}
		(*len_b)++;
		(*len_a)--;
	}
}

void	rotate_modified(int *stack, int len)
{
	int	aux;
	int	i;

	i = 0;
	aux = stack[0];
	while (i < (len - 1))
	{
		stack[i] = stack[i + 1];
		i++;
	}
	stack[len - 1] = aux;
}
