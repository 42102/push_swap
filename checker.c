/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/01 19:26:29 by anramire          #+#    #+#             */
/*   Updated: 2022/06/02 19:38:30 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_push_swap.h"
#include "./ft_get_next_line/get_next_line.h"
#include "libft/libft.h"
#include <stdio.h>

void	reading(int *stack_a, int *stack_b, int *len_a, int *len_b);
void	check_command(int *stack, char *str);
void	print_error(int *stack);
void	do_a_commands(int *stack_a, int *len_a, char *cmd);

int	main(int argc, char *argv[])
{
	int	len_a;
	int	len_b;
	int	*stack_a;
	int	*stack_b;

	if (argc > 1)
	{
		get_stack(argv, argc, &stack_a, &len_a);
		check_stack(stack_a, len_a);
		stack_b = initialize_stack(len_a);
		len_b = 0;
		converse_number(stack_a, len_a);
		reading(stack_a, stack_b, &len_a, &len_b);
		free(stack_a);
		free(stack_b);
	}
	return (0);
}

void	reading(int *stack_a, int *stack_b, int *len_a, int *len_b)
{
	char	*command;
	int		fd;

	fd = 0;
	command = get_next_line(fd);
	while (command != NULL)
	{
		check_command(stack_a, command);
		do_a_commands(stack_a, len_a, command);
		do_b_commands(stack_b, len_b, command);
		if (ft_strcmp(command, "rrr\n") == 0)
			reverse_rotate_both_m(stack_a, stack_b, *len_a, *len_b);
		if (ft_strcmp(command, "ss\n") == 0)
			swapss_modified(stack_a, stack_b, *len_a, *len_b);
		if (ft_strcmp(command, "rr\n") == 0)
			rotate_both_mod(stack_a, stack_b, *len_a, *len_b);
		if (ft_strcmp(command, "pa\n") == 0)
			push_a_modified(stack_a, stack_b, len_a, len_b);
		if (ft_strcmp(command, "pb\n") == 0)
			push_b_modified(stack_a, stack_b, len_a, len_b);
		command = get_next_line(fd);
	}
	is_sorted_checker(stack_a, *len_a);
}

void	check_command(int *stack, char *str)
{
	if (ft_strcmp(str, "rra\n") == 0)
		return ;
	if (ft_strcmp(str, "rrb\n") == 0)
		return ;
	if (ft_strcmp(str, "rrr\n") == 0)
		return ;
	if (ft_strcmp(str, "sa\n") == 0)
		return ;
	if (ft_strcmp(str, "sb\n") == 0)
		return ;
	if (ft_strcmp(str, "ss\n") == 0)
		return ;
	if (ft_strcmp(str, "ra\n") == 0)
		return ;
	if (ft_strcmp(str, "rb\n") == 0)
		return ;
	if (ft_strcmp(str, "rr\n") == 0)
		return ;
	if (ft_strcmp(str, "pa\n") == 0)
		return ;
	if (ft_strcmp(str, "pb\n") == 0)
		return ;
	print_error(stack);
}

void	print_error(int *stack)
{
	free(stack);
	ft_putstr_fd("Error\n", 1);
	exit(-1);
}

void	do_a_commands(int *stack_a, int *len_a, char *cmd)
{
	if (ft_strcmp(cmd, "rra\n") == 0)
		reverse_rotate_modified(stack_a, *len_a);
	if (ft_strcmp(cmd, "sa\n") == 0)
		swap_modified(stack_a, *len_a);
	if (ft_strcmp(cmd, "ra\n") == 0)
		rotate_modified(stack_a, *len_a);
}
